<?php

namespace App\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class LuckyController
{
    /**
     * @Route("/solve/{a}/{b}/{c}", name="app_lucky_number")
     */
    public function solve($a,$b,$c)
    {
        if ($a==0)
        {
            return new Response(
            '<html><body>Это не квадратное уравнение (коэффициент у x^2 нулевой)</body></html>'
            );
        }
        else
        {
            $d=$b*$b-4*$a*$c;
            
            if ($d<0)
            {
                return new Response(
                '<html><body>Решений в действительных числах нет, поскольку D<0</body></html>'
                );
            }
            else
            {
                $d=sqrt($d);
                $x1=((-$b)+$d)/(2*$a);
                $x2=((-$b)-$d)/(2*$a);
                return new Response(
                '<html><body>Solution of '.$a.' x^2 + '.$b.' x + '.$c.' is: x_1 = '.$x1.' and x_2 = '.$x2.'</body></html>'
                );
            }
        }   
    }
}

?>
