rm -rf ~/gurren_lagann
cp -r gurren_lagann_staging/ gurren_lagann/
cd ~/gurren_lagann
docker-compose down
docker-compose exec fpm composer install
sed -i 's/8098/8099/' docker-compose.yml
sed -i 's/33061/33062/' docker-compose.yml
composer install
docker-compose up -d